/*package main

import "fmt"

var estado bool

func main() {
	estado = true
	//Si hay un else en un if debe de ir unido a la llaven anterior y siguiendo una llave sino da error
	// if estado=false; estado ==true {}else{}
	if estado == true { //en la instruccion del if se puede asignar o cambiar el valor de una variable
		fmt.Println(estado)
	}
}
*/

//switch

package main

import "fmt"

var estado bool

func main(){

	switch numero:= 6; numero{
	case 1:
		fmt.Println(1)
	case 2:
		fmt.Println(2)
	case 3:
		fmt.Println(3)
	case 4:
		fmt.Println(4)
	case 5:
		fmt.Println(5)
	default:
		fmt.Println("Mayor a 5")
	}
}
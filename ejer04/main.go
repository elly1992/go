package main

import (
	"bufio"
	"fmt"
	"os"
) 

var numero1 int
var numero2 int
var resultado int
var leyenda string

func main(){
	fmt.Println("Ingrese numero 1: ")
	fmt.Scanln(&numero1) // verbo %d (numerico base 10 es la d)

	fmt.Println("Ingrese numero 2: ")
	fmt.Scanln(&numero2)

	fmt.Println("Descripcion : ")

	scanner :=bufio.NewScanner(os.Stdin) //tenemos un objeto de tipo scanner, del os se extraera del modo standard de entrada (teclado)
	if scanner.Scan(){
		leyenda = scanner.Text()
	}

	resultado = numero1 + numero2
	fmt.Println(leyenda, resultado)
}
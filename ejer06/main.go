//en el curso este es el ejer05

package main

import "fmt"

func main(){
	fmt.Println(Calculo(5,46))
	fmt.Println(Calculo(2,23,45,68))
	fmt.Println(Calculo(5))
	fmt.Println(Calculo(5,46,17,25,26,98,17))

	//numero, estado := dos(1) //estamos declarando dos variables y las estamos inicializando con los 2 valores que nos retornan de la función dos()
	//si la función develve 2 valores, deben haber 2 variables a la izquierda para almacenar los valores
	//fmt.Println(numero)
	//fmt.Println(estado)
}

func uno(numero int) int {
	return numero *2
}

func dos(numero int) (int, bool) {
	if numero == 1{
		return 5, false
	} else {
		return 10, true
	}
}

func Calculo(numero ...int) int{ //le estamos diciendo con los "...", que va a recibir varios parametros enteros, pero no sabemos cuantos
	total :=0
	for item, num :=range numero{ //el comando range devuelce siempre 2 valores, toma un rango de parametros y lo que se coloca a la derecha es un vector una lista
		//el primer valor que devuelve es el numero del elemento (1 o 2 o 3)
		//cuando hay una funcion que devuelve algun tipo de dato que yo no quiero usar se colocal el guion bajo (_) y ahi se alojara la variable 
		total = total + num
		fmt.Printf("\n Item %d \n", item)
	}
	return total
}
package main

import "fmt"

/*
func main(){
	var i = 0
	for i < 10 {
		fmt.Printf("\n Valor de i: %d", i)
		if i == 5 {
			fmt.Printf(" multiplicamos 2 \n")
			i=i*2
			fmt.Printf("\n Valor de i: %d", i)
			continue //este palabra hace que ahi se detenga el ciclo
		}
		fmt.Printf("    Paso por aqui \n")
		i++
	}
} */

func main(){
	var i int = 0

	RUTINA:
	for i < 10{
		if i == 4 {
			i= i+2
			fmt.Println("Voy a RUTINA sumando 2 a i")
			goto RUTINA // esto es como el continue pero en lugar de ir al inicio del for va a RUTINA linea 24 o a donde le indiquemos
		}
		fmt.Printf("Valor de i: %d\n",i)
		i++
	}
}